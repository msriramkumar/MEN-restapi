const express = require('express');
const mongoose = require('mongoose');
const post_router = require('./routes/posts');
const comment_router = require('./routes/comments');
var app = express();

//Establishing connection with MongoDb Hosted On The Cloud
mongoose.connect('mongodb://user:123@ds013579.mlab.com:13579/testdb1',{
  useMongoClient:true
});

//Loading In Router Middlewares
app.use('/api/posts',post_router)
app.use('/api/comments',comment_router)


//Root Assignment
app.get('/',function (req,res) {
  res.sendFile(__dirname + "/index.html")
})


//Port Assignment
app.listen(3000);
console.log('running on port 3000');
