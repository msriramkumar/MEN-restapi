const express = require('express')
const posts = require('../models/posts');
const bodyParser = require('body-parser');
var router = express.Router()


router.use(bodyParser.json())

//requests made to /api/posts:

router.get('/',function(req,res){
    posts.find({},function(err,postresults){
    if(err)
    {
      throw err;
    }
    res.json(postresults)
  })
})

router.get('/:id',function(req,res) {
  posts.find({id:req.params.id},function (err,postresults) {
    if (err) {
      throw err
    }
    res.json(postresults)
  })

})

//Inserting data...

router.post('/',function (req,res) {
  var new_post = new posts(req.body);
  var promise = new_post.save();

  promise.then(function(savedpost){
    res.json(savedpost)
  })
})

//Updating data

router.put('/',function (req,res) {
  posts.findOneAndUpdate(
    {
      id:req.body.id
    },
    {
      title : req.body.title,
      body : req.body.body
    },
    function (err,update) {
      if(err) throw err;

      res.json(update)
  })
})

//Deletng data

router.delete('/:id',function (req,res) {

  posts.findOneAndRemove(
  {
    id: req.params.id
  },
  function (err,deleted) {
    if(err) throw err

    res.json(deleted);
  })

})

module.exports = router
