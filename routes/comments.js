const express = require('express')
const bodyParser = require('body-parser');
const comments = require('../models/comments');
var router = express.Router()


router.use(bodyParser.json())
//requests made to /api/posts:

router.get('/',function(req,res){
    comments.find({},function(err,commentresults){
    if(err)
    {
      throw err;
    }
    res.json(commentresults)
  })
})

router.get('/:id',function(req,res) {
  comments.find({id:req.params.id},function (err,commentresults) {
    if (err) {
      throw err
    }
    res.json(commentresults)
  })

})

//Inserting data...

router.post('/',function (req,res) {

  var comment = new comments(req.body)
  var promise = comment.save();

  promise.then(function (savedcomment) {
    res.json(savedcomment)
  })

})

//Updating Data
router.put('/',function (req,res) {
  comments.findOneAndUpdate(
    {
      id:req.body.id
    },
    {
      name : req.body.name,
      email : req.body.email,
      body : req.body.body
    },
    function (err,update) {
      if(err) throw err;

      res.json(update)
  })
})

//Deleting data

router.delete('/:id',function (req,res) {
  comments.findOneAndRemove(
    {
      id : req.params.id
    },
    function (err,deleted) {

    if(err) throw err

    res.json(deleted)
  })

})

module.exports = router
