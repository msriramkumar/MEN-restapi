const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var postSchema = new Schema({
  id:{
    type : Number,
    required : true,
    unique : true
  },
  title:{
    type : String,
    required : true
  },
  body:{
    type : String,
    required : true
  }
});

var posts = mongoose.model("posts",postSchema);

module.exports = posts;
