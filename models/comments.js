const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentSchema = new Schema({
  id:{
    type : Number,
    required : true,
    unique : true
  },
  name:{
    type : String,
    required : true
  },
  email:{
    type : String,
    required : true
  },
  body:{
    type : String,
    required : true
  }
});

var comments = mongoose.model("comments",commentSchema);

module.exports = comments;
